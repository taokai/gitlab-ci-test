#!/usr/bin/python3
 
import os

print("Hello, World!")
commit  = os.popen('git rev-parse HEAD').read().strip()
origins = os.popen('git ls-remote origin').read().strip()
isHeadingBranch = False

if commit in origins:
    origins = origins.split('\n')
    for x in origins:
        if commit in x and 'refs/heads/' in x:
            branch = x.split('\trefs/heads/')[1]
            isHeadingBranch = True

print(isHeadingBranch,branch)